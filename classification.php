<?php 
header("Access-Control-Allow-Origin: *"); 
header("Access-Control-Allow-Headers: Content-Type"); ?> <?php

require_once('settings.php');

$query = $_GET["query"];
$tweets = getTweetsWith($query);

$classification = array();
foreach ($tweets as &$tweet) {
	$result = $DatumboxAPI->TopicClassification($tweet);
	if (!isset($classification[$result])) {
	    $classification[$result] = 0;
	}
	$classification[$result]++;
}

unset($DatumboxAPI);

print json_encode($classification);